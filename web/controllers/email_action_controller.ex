defmodule IotData.EmailActionController do
  use IotData.Web, :controller

  alias IotData.EmailAction

  plug :scrub_params, "email_action" when action in [:create, :update]
  plug :check_ownership when action in [:show, :edit, :update]

  def index(conn, _params) do
    user_id = get_session(conn, :user_id)
    email_actions = Repo.all from e in EmailAction, where: e.user_id == ^user_id
    render(conn, "index.html", email_actions: email_actions)
  end

  def new(conn, _params) do
    changeset = EmailAction.changeset(%EmailAction{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"email_action" => email_action_params}) do
    changeset = EmailAction.changeset(%EmailAction{}, Map.put(email_action_params, "user_id", get_session(conn, :user_id)))

    case Repo.insert(changeset) do
      {:ok, _email_action} ->
        conn
        |> put_flash(:info, "Email action created successfully.")
        |> redirect(to: email_action_path(conn, :index))
      {:error, changeset} ->
        IO.inspect changeset
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    email_action = Repo.get!(EmailAction, id)
    render(conn, "show.html", email_action: email_action)
  end

  def edit(conn, %{"id" => id}) do
    email_action = Repo.get!(EmailAction, id)
    changeset = EmailAction.changeset(email_action)
    render(conn, "edit.html", email_action: email_action, changeset: changeset)
  end

  def update(conn, %{"id" => id, "email_action" => email_action_params}) do
    email_action = Repo.get!(EmailAction, id)
    changeset = EmailAction.changeset(email_action, email_action_params)

    case Repo.update(changeset) do
      {:ok, email_action} ->
        conn
        |> put_flash(:info, "Email action updated successfully.")
        |> redirect(to: email_action_path(conn, :show, email_action))
      {:error, changeset} ->
        render(conn, "edit.html", email_action: email_action, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    email_action = Repo.get!(EmailAction, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(email_action)

    conn
    |> put_flash(:info, "Email action deleted successfully.")
    |> redirect(to: email_action_path(conn, :index))
  end

  defp check_ownership(conn, _opts) do
    user = conn.assigns[:current_user]
    email_action = Repo.get!(EmailAction, conn.params["id"])
    if email_action.user_id == user.id do
      conn
    else
      conn
      |> redirect(to: email_action_path(conn, :index))
    end
  end
end
