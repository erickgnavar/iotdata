defmodule IotData.UserController do
  use IotData.Web, :controller

  alias IotData.User

  plug :scrub_params, "user" when action in [:create, :update]

  def new(conn, _params) do
    changeset = User.registration_changeset(%User{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"user" => user_params}) do
    changeset = User.registration_changeset(%User{}, user_params)

    case Repo.insert(changeset) do
      {:ok, _user} ->
        conn
        |> put_flash(:info, gettext("User created successfully"))
        |> redirect(to: user_path(conn, :show))
      {:error, changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def edit(conn, _) do
    user = conn.assigns[:current_user]
    changeset = User.edit_changeset(user)
    render(conn, "edit.html", user: user, changeset: changeset)
  end

  def update(conn, %{"user" => user_params}) do
    user = conn.assigns[:current_user]
    changeset = User.edit_changeset(user, user_params)

    case Repo.update(changeset) do
      {:ok, user} ->
        conn
        |> put_flash(:info, gettext("User updated successfully"))
        |> redirect(to: user_path(conn, :show))
      {:error, changeset} ->
        render(conn, "edit.html", user: user, changeset: changeset)
    end
  end

  def show(conn, _) do
    render(conn, "show.html", user: conn.assigns[:current_user])
  end

  def generate_api_key(conn, _) do
    user = conn.assigns[:current_user]
    changeset = Ecto.Changeset.change(user, api_key: User.generate_api_key)
    Repo.update(changeset)
    redirect(conn, to: user_path(conn, :show))
  end
end
