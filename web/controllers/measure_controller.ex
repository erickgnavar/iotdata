defmodule IotData.MeasureController do
  use IotData.Web, :controller

  alias IotData.{Feed, Measure}

  plug :check_api_key, "feed" when action in [:index, :create]

  def index(conn, %{"feed_id" => feed_id}) do
    measures = Repo.all from m in Measure, where: m.feed_id == ^feed_id
    render(conn, "index.json", measures: measures)
  end

  def create(conn, %{"value" => value}) do
    feed_id = conn.assigns[:feed].id
    params = %{
      "feed_id" => feed_id,
      "value" => to_string(value)
    }
    changeset = Measure.changeset(%Measure{}, params)
    case Repo.insert(changeset) do
      {:ok, measure} ->
        IotData.Publisher.publish("measures:new:#{measure.id}")
        conn
        |> put_status(:created)
        |> render("show.json", measure: measure)
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(IotData.ChangesetView, "error.json", changeset: changeset)
    end
  end

  def export(conn, %{"feed_id" => feed_id}) do
    measures = Repo.all from m in Measure, where: m.feed_id == ^feed_id
    feed = Repo.get!(Feed, feed_id)
    lines = ["inserted_at, value"] ++ Enum.map(measures, fn m -> "#{m.inserted_at}, #{m.value}" end)
    conn
    |> put_resp_content_type("text/csv")
    |> put_resp_header("content-disposition", "attachment; filename=#{feed.name}.csv")
    |> send_resp(200, Enum.join(lines, "\n"))
  end

  def check_api_key(conn, _opts) do
    case get_req_header(conn, "x-api-key") do
      [api_key] when is_binary(api_key) ->
        feed = Repo.get_by(IotData.Feed, api_key: api_key)
        if feed == nil do
          conn |> denied(:unauthorized)
        else
          conn |> assign(:feed, feed)
        end
      _ ->
        conn |> denied(:bad_request)
    end
  end

  defp denied(conn, reason) do
    conn
    |> put_status(reason)
    |> Phoenix.Controller.json(%{message: "Api required"})
    |> halt
  end
end
