defmodule IotData.FeedController do
  use IotData.Web, :controller

  alias IotData.Feed

  plug :scrub_params, "feed" when action in [:create, :update]
  plug :check_ownership when action in [:show, :edit, :update, :generate_api_key]

  def index(conn, _params) do
    user = conn.assigns[:current_user]
    feeds = Repo.all from d in Feed, where: d.user_id == ^user.id
    render(conn, "index.html", feeds: feeds)
  end

  def new(conn, _params) do
    changeset = Feed.create_changeset(%Feed{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"feed" => feed_params}) do
    user = conn.assigns[:current_user]
    changeset = Feed.create_changeset(%Feed{}, Map.put(feed_params, "user_id", user.id))

    case Repo.insert(changeset) do
      {:ok, _feed} ->
        conn
        |> put_flash(:info, gettext("Feed created successfully"))
        |> redirect(to: feed_path(conn, :index))
      {:error, changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    feed = Repo.get!(Feed, id)
    if conn.port in [80, 443] do
      port = ""
    else
      port = ":#{conn.port}"
    end
    render(conn, "show.html", feed: feed, url: "#{conn.scheme}://#{conn.host}#{port}")
  end

  def edit(conn, %{"id" => id}) do
    feed = Repo.get!(Feed, id)
    changeset = Feed.edit_changeset(feed)
    render(conn, "edit.html", feed: feed, changeset: changeset)
  end

  def update(conn, %{"id" => id, "feed" => feed_params}) do
    feed = Repo.get!(Feed, id)
    user = conn.assigns[:current_user]
    changeset = Feed.edit_changeset(feed, Map.put(feed_params, "user_id", user.id))

    case Repo.update(changeset) do
      {:ok, feed} ->
        conn
        |> put_flash(:info, gettext("Feed updated successfully"))
        |> redirect(to: feed_path(conn, :show, feed))
      {:error, changeset} ->
        render(conn, "edit.html", feed: feed, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    feed = Repo.get!(Feed, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(feed)

    conn
    |> put_flash(:info, gettext("Feed deleted successfully"))
    |> redirect(to: feed_path(conn, :index))
  end

  def generate_api_key(conn, %{"id" => id}) do
    feed = Repo.get!(Feed, id)
    changeset = Ecto.Changeset.change(feed, api_key: Feed.generate_api_key)
    Repo.update(changeset)
    redirect(conn, to: feed_path(conn, :show, feed))
  end

  defp check_ownership(conn, _opts) do
    user = conn.assigns[:current_user]
    feed = Repo.get!(Feed, conn.params["id"])
    if feed.user_id == user.id do
      conn
    else
      conn
      |> redirect(to: feed_path(conn, :index))
    end
  end
end
