defmodule IotData.PageController do
  use IotData.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
