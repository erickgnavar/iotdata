defmodule IotData.TriggerController do
  use IotData.Web, :controller

  alias IotData.{Feed, Trigger}

  plug :scrub_params, "trigger" when action in [:create, :update]

  @data_type_choices [
    {gettext("Numeric"), "numeric"},
    {gettext("String"), "string"}
  ]

  @condition_choices [
    {gettext("greater than"), "gt"},
    {gettext("greater than or equal to"), "gte"},
    {gettext("lower than"), "lt"},
    {gettext("greater than or equal to"), "lte"},
    {gettext("equals"), "eq"},
    {gettext("not equals"), "ne"}
  ]

  def index(conn, _params) do
    user_id = get_session(conn, :user_id)
    IO.inspect user_id
    triggers = Repo.all from r in Trigger, where: r.user_id == ^user_id, preload: [:feed]
    render(conn, "index.html", triggers: triggers)
  end

  def new(conn, _params) do
    changeset = Trigger.changeset(%Trigger{})
    feed_choices = gen_feed_choices(conn)
    render(conn, "new.html", changeset: changeset, condition_choices: @condition_choices, data_type_choices: @data_type_choices, feed_choices: feed_choices)
  end

  def create(conn, %{"trigger" => trigger_params}) do
    changeset = Trigger.changeset(%Trigger{}, Map.put(trigger_params, "user_id", get_session(conn, :user_id)))

    case Repo.insert(changeset) do
      {:ok, _trigger} ->
        conn
        |> put_flash(:info, "Trigger created successfully.")
        |> redirect(to: trigger_path(conn, :index))
      {:error, changeset} ->
        feed_choices = gen_feed_choices(conn)
        render(conn, "new.html", changeset: changeset, condition_choices: @condition_choices, data_type_choices: @data_type_choices, feed_choices: feed_choices)
    end
  end

  def show(conn, %{"id" => id}) do
    trigger = Repo.get!(Trigger, id)
    render(conn, "show.html", trigger: trigger)
  end

  def edit(conn, %{"id" => id}) do
    trigger = Repo.get!(Trigger, id)
    changeset = Trigger.changeset(trigger)
    feed_choices = gen_feed_choices(conn)
    render(conn, "edit.html", trigger: trigger, changeset: changeset, condition_choices: @condition_choices, data_type_choices: @data_type_choices, feed_choices: feed_choices)
  end

  def update(conn, %{"id" => id, "trigger" => trigger_params}) do
    trigger = Repo.get!(Trigger, id)
    changeset = Trigger.changeset(trigger, trigger_params)

    case Repo.update(changeset) do
      {:ok, trigger} ->
        conn
        |> put_flash(:info, gettext("Trigger updated successfully."))
        |> redirect(to: trigger_path(conn, :show, trigger))
      {:error, changeset} ->
        feed_choices = gen_feed_choices(conn)
        render(conn, "edit.html", trigger: trigger, changeset: changeset, condition_choices: @condition_choices, data_type_choices: @data_type_choices, feed_choices: feed_choices)
    end
  end

  def delete(conn, %{"id" => id}) do
    trigger = Repo.get!(Trigger, id)

    Repo.delete!(trigger)

    conn
    |> put_flash(:info, gettext("Trigger deleted successfully."))
    |> redirect(to: trigger_path(conn, :index))
  end

  defp gen_feed_choices(conn) do
    import Ecto.Query
    Feed
    |> select([d], {d.name, d.id})
    |> where(user_id: ^get_session(conn, :user_id))
    |> Repo.all
  end
end
