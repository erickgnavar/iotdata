defmodule IotData.SessionController do
  use IotData.Web, :controller

  alias IotData.Session

  def new(conn, _) do
    render(conn, "new.html")
  end

  def create(conn, %{"session" => session_params}) do
    IO.inspect session_params
    case Session.authenticate(conn, session_params) do
      {:ok, conn} ->
        conn
        |> put_flash(:info, "welcome back")
        |> redirect(to: feed_path(conn, :index))
      {:error, _reason, conn} ->
        conn
        |> put_flash(:error, "Invalid email or password")
        |> render("new.html")
    end
  end

  def delete(conn, _) do
    conn
    |> Session.logout
    |> put_flash(:info, "logged out")
    |> redirect(to: page_path(conn, :index))
  end
end
