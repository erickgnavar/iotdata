defmodule IotData.Measure do
  use IotData.Web, :model

  schema "measures" do
    field :value, :string

    belongs_to :feed, IotData.Feed

    timestamps
  end

  @required_fields ~w(feed_id value)
  @optional_fields ~w()

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields, @optional_fields)
    |> foreign_key_constraint(:feed_id)
  end
end
