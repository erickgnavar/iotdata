defmodule IotData.EmailAction do
  use IotData.Web, :model

  schema "email_actions" do
    field :recipient, :string
    field :subject, :string
    field :body, :string
    belongs_to :user, IotData.User

    timestamps
  end

  @required_fields ~w(recipient subject body user_id)
  @optional_fields ~w()

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields, @optional_fields)
    |> validate_format(:recipient, ~r/@/)
    |> validate_length(:subject, min: 1)
    |> foreign_key_constraint(:user_id)
  end

  def run(action, value) do
    body = String.replace(action.body, "${value}", to_string(value))
    IotData.Mailer.send_message(action.subject, action.recipient, body)
  end
end
