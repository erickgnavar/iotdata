defmodule IotData.Session do
  import Comeonin.Bcrypt, only: [checkpw: 2]
  import Plug.Conn

  alias IotData.Repo

  def authenticate(conn, %{"email" => email, "password" => given_password}) do
    user = Repo.get_by(IotData.User, email: email)
    cond do
      user && checkpw(given_password, user.password_hash) ->
        {:ok, login(conn, user)}
      user ->
        {:error, :unauthorized, conn}
      true ->
        {:error, :not_found, conn}
    end
  end

  defp login(conn, user) do
    conn
    |> put_session(:user_id, user.id)
    |> assign(:current_user, user)
    |> configure_session(renew: true)
  end

  def logout(conn) do
    configure_session(conn, drop: true)
  end
end
