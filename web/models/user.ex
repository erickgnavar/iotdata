defmodule IotData.User do
  use IotData.Web, :model
  import IotData.Gettext

  schema "users" do
    field :first_name, :string
    field :last_name, :string
    field :email, :string
    field :username, :string
    field :password, :string, virtual: true
    field :password_hash, :string
    field :api_key, :string
    field :time_zone, :string

    has_many :feeds, IotData.Feed

    timestamps
  end

  def changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields, @optional_fields)
  end

  def registration_changeset(model, params \\ :empty) do
    model
    |> cast(params, ~w(email username password), [])
    |> validate_format(:email, ~r/@/)
    |> unique_constraint(:username, message: gettext("Username taken"))
    |> unique_constraint(:email, message: gettext("Email already used"))
    |> validate_length(:password, min: 5)
    |> put_password_hash
    |> put_api_key
  end

  def edit_changeset(model, params \\ :empty) do
    model
    |> cast(params, ~w(email username), ~w(first_name last_name time_zone))
    |> validate_format(:email, ~r/@/)
    |> unique_constraint(:username, message: gettext("Username taken"))
    |> unique_constraint(:email, message: gettext("Email already used"))
  end

  def generate_api_key do
    Ecto.UUID.generate |> String.replace("-", "")
  end

  defp put_password_hash(changeset) do
    if changeset.valid? do
      changeset
      |> put_change(:password_hash, Comeonin.Bcrypt.hashpwsalt(changeset.changes.password))
    else
      changeset
    end
  end

  defp put_api_key(changeset) do
    if changeset.valid? do
      changeset |> put_change(:api_key, generate_api_key)
    else
      changeset
    end
  end
end
