defmodule IotData.Trigger do
  use IotData.Web, :model
  require Logger

  schema "triggers" do
    field :name, :string
    field :data_type, :string
    field :condition, :string
    field :comparison_value, :string
    field :action_type, :string
    field :action_id, :integer

    belongs_to :user, IotData.User
    belongs_to :feed, IotData.Feed

    timestamps
  end

  @required_fields ~w(name data_type feed_id condition comparison_value action_type action_id user_id)
  @optional_fields ~w()

  def changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields, @optional_fields)
    |> validate_inclusion(:data_type, ["numeric", "string"])
    |> validate_inclusion(:condition, ["gt", "gte", "lt", "lte", "eq", "ne"])
    |> foreign_key_constraint(:user_id)
    |> foreign_key_constraint(:feed_id)
  end

  def call_action(trigger, value) do
    alias IotData.{Repo, EmailAction}
    actions_map = %{
      "email" => EmailAction
    }
    action_module = Map.get(actions_map, trigger.action_type)
    action = Repo.get!(action_module, trigger.action_id)
    action_module.run(action, value)
  end

  def evaluate(trigger, value) do
    Logger.debug "evaluation: #{value} #{trigger.condition} #{trigger.comparison_value}"
    condition = String.to_atom(trigger.condition)
    case trigger.data_type do
      "numeric" ->
        {comparison_value, _} = Float.parse(trigger.comparison_value)
        {value, _} = Float.parse(value)
        eval(condition, value, comparison_value)
      "string" ->
        eval(condition, value, trigger.comparison_value)
    end
  end

  defp eval(:gt, measure_value, comparison_value) do
    measure_value > comparison_value
  end

  defp eval(:gte, measure_value, comparison_value) do
    measure_value >= comparison_value
  end

  defp eval(:lt, measure_value, comparison_value) do
    measure_value < comparison_value
  end

  defp eval(:lte, measure_value, comparison_value) do
    measure_value <= comparison_value
  end

  defp eval(:eq, measure_value, comparison_value) do
    measure_value == comparison_value
  end

  defp eval(:ne, measure_value, comparison_value) do
    measure_value != comparison_value
  end

  defp eval(:contains, measure_value, comparison_value) do
    String.contains? measure_value, comparison_value
  end

  defp eval(:starts_with, measure_value, comparison_value) do
    String.starts_with? measure_value, comparison_value
  end

  defp eval(:ends_with, measure_value, comparison_value) do
    String.ends_with? measure_value, comparison_value
  end

  defp eval(opt, _measure_value, _comparison_value) do
    Logger.debug(opt)
  end
end
