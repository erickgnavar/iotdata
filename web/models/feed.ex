defmodule IotData.Feed do
  use IotData.Web, :model

  schema "feeds" do
    field :name, :string
    field :description, :string
    field :latitude, :float
    field :longitude, :float
    field :api_key, :string

    belongs_to :user, IotData.User
    has_many :measures, IotData.Measure

    timestamps
  end

  @required_fields ~w(name user_id)
  @optional_fields ~w(description latitude longitude)

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """

  def create_changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields, @optional_fields)
    |> foreign_key_constraint(:user_id)
    |> put_api_key
  end

  def edit_changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields, @optional_fields)
    |> foreign_key_constraint(:user_id)
  end

  def generate_api_key do
    Ecto.UUID.generate |> String.replace("-", "")
  end

  defp put_api_key(changeset) do
    if changeset.valid? do
      changeset |> put_change(:api_key, generate_api_key)
    else
      changeset
    end
  end
end
