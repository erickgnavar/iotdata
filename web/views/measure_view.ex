defmodule IotData.MeasureView do
  use IotData.Web, :view

  def render("index.json", %{measures: measures}) do
    %{data: render_many(measures, IotData.MeasureView, "measure.json")}
  end

  def render("show.json", %{measure: measure}) do
    %{data: render_one(measure, IotData.MeasureView, "measure.json")}
  end

  def render("measure.json", %{measure: measure}) do
    %{
      id: measure.id,
      feed_id: measure.feed_id,
      value: measure.value,
      inserted_at: measure.inserted_at
    }
  end
end
