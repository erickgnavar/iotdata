import socket from './socket'

window.d3 = require('d3')
require('epoch-charting')

let channel = socket.channel(`feeds:${window.feed_id}`, {})
channel.join()
  .receive('ok', resp => { console.log('Joined successfully', resp) })
  .receive('error', resp => { console.log('Unable to join', resp) })

let chart = null

$.ajax({
  url: `/api/feeds/${window.feed_id}/data/`,
  method: 'get',
  dataType: 'json',
  headers: {
    'x-api-key': window.feed_api_key
  },
  success: response => {
    let chartData = [
      {
        values: response.data.map(prepareData)
      }
    ]
    chart = $('#chart').epoch({
      type: 'time.line',
      data: chartData,
      axes: ['left', 'bottom', 'right']
    })
  }
})

channel.on('new_measure', response => {
  chart.push([prepareData(response)])
})

function prepareData(item) {
  return {
    time: (new Date(item.inserted_at)).getTime(),
    y: item.value
  }
}
