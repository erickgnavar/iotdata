defmodule IotData.FeedChannel do
  use IotData.Web, :channel
  require Logger

  @queue "iot_queue"
  @exchange "iot_exchange"

  def join("feeds:lobby", payload, socket) do
    if authorized?(payload) do
      {:ok, socket}
    else
      {:error, %{reason: "unauthorized"}}
    end
  end

  def join("feeds:" <> feed_id, _payload, socket) do
    Logger.info "joined to: #{feed_id}"
    send self, :after_join
    socket = assign(socket, :feed_id, feed_id)
    {:ok, socket}
  end

  def handle_info(:after_join, socket) do
    pid = spawn __MODULE__, :listen, [socket]
    socket = assign(socket, :listen_pid, pid)
    {:noreply, socket}
  end

  def terminate(_reason, socket) do
    pid = socket.assigns[:listen_pid]
    Process.exit pid, :kill
  end

  defp authorized?(_payload) do
    true
  end

  def listen(socket) do
    {:ok, conn} = AMQP.Connection.open
    {:ok, channel} = AMQP.Channel.open(conn)

    AMQP.Queue.declare channel, @queue
    AMQP.Exchange.declare channel, @exchange, :fanout
    AMQP.Queue.bind channel, @queue, @exchange
    AMQP.Queue.subscribe channel, @queue, fn payload, _meta ->
      case payload do
        "measures:new:" <> measure_id ->
          measure = Repo.get!(IotData.Measure, measure_id)
          data = IotData.MeasureView.render("measure.json", %{measure: measure})
          push socket, "new_measure", data
      end
    end
  end
end
