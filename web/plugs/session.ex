defmodule IotData.Plugs.Session do
  import Plug.Conn

  def init(opts), do: opts

  def call(conn, _opts) do
    case conn.assigns[:current_user] do
      nil ->
        conn
        |> Phoenix.Controller.redirect(to: "/login/")
      _ ->
        conn
    end
  end
end
