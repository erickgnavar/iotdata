defmodule IotData.Plugs.Locale do
  import Plug.Conn

  def init(opts), do: opts

  def call(conn, _opts) do
    case conn.params["locale"] || get_session(conn, :locale) do
      nil ->
        conn
      locale when is_binary(locale) ->
        Gettext.put_locale(IotData.Gettext, locale)
        conn |> put_session(:locale, locale)
    end
  end
end
