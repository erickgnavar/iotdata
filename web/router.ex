defmodule IotData.Router do
  use IotData.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug IotData.Plugs.Auth, repo: IotData.Repo
    plug IotData.Plugs.Locale
  end

  pipeline :login_required do
    plug IotData.Plugs.Session
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", IotData do
    pipe_through :browser

    get "/", PageController, :index
    get "/register", UserController, :new
    post "/register", UserController, :create
    get "/login", SessionController, :new
    post "/login", SessionController, :create
    get "/logout", SessionController, :delete
  end

  scope "/", IotData do
    pipe_through :browser
    pipe_through :login_required

    get "/account", UserController, :show
    get "/account/edit", UserController, :edit
    put "/account/edit", UserController, :update
    put "/account/generate_api_key", UserController, :generate_api_key
    resources "/feeds", FeedController
    put "/feeds/:id/generate_api_key", FeedController, :generate_api_key
    get "/feeds/:feed_id/export", MeasureController, :export
    resources "/triggers", TriggerController
    resources "/email_actions", EmailActionController
  end

  scope "/api", IotData do
    pipe_through :api

    get "/feeds/:feed_id/data", MeasureController, :index
    post "/feeds", MeasureController, :create
  end
end
