defmodule IotData.Publisher do
  use GenServer
  alias AMQP.{Queue, Exchange, Connection, Channel, Basic}
  require Logger

  @queue "iot_queue"
  @exchange "iot_exchange"

  # public API

  def start_link do
    GenServer.start_link(__MODULE__, [], name: __MODULE__)
  end

  def publish(message) do
    GenServer.cast(__MODULE__, {:publish, message})
  end

  # private API

  def init(_opts) do
    Logger.info("publisher worker started")
    {:ok, conn} = Connection.open
    {:ok, channel} = Channel.open(conn)

    Queue.declare channel, @queue
    Exchange.declare channel, @exchange, :fanout
    Queue.bind channel, @queue, @exchange
    {:ok, channel}
  end

  def handle_cast({:publish, message}, channel) do
    Logger.debug "published message: #{message}"
    Basic.publish channel, @exchange, "", message
    {:noreply, channel}
  end
end
