defmodule IotData.SocketNotifierWorker do
  use GenServer
  alias AMQP.{Queue, Exchange, Connection, Channel}
  require Logger

  @queue "iot_queue"
  @exchange "iot_exchange"

  def start_link do
    GenServer.start_link(__MODULE__, [], name: __MODULE__)
  end

  def init(_opts) do
    Logger.info "SocketNotifierWorker started"
    {:ok, conn} = Connection.open
    {:ok, channel} = Channel.open(conn)

    Queue.declare channel, @queue
    Exchange.declare channel, @exchange, :fanout
    Queue.bind channel, @queue, @exchange
    Queue.subscribe channel, @queue, fn (payload, _meta) ->
      Logger.debug "notifier worker: #{payload}"
      case payload do
        "measures:new:" <> measure_id ->
          spawn fn -> notify(measure_id) end
      end
    end
    {:ok, channel}
  end

  def notify(measure_id) do
    alias IotData.{Repo, Trigger, Measure}
    measure = Repo.get!(IotData.Measure, measure_id)
    data = IotData.MeasureView.render("measure.json", %{measure: measure})
    topic = "feeds:#{measure.feed_id}"
    IotData.Endpoint.broadcast(topic, "new_measure", data)
  end
end
