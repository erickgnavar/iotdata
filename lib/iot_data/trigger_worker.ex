defmodule IotData.TriggerWorker do
  use GenServer
  alias AMQP.{Queue, Exchange, Connection, Channel}
  require Logger

  @queue "iot_trigger_queue"
  @exchange "iot_exchange"

  def start_link do
    GenServer.start_link(__MODULE__, [], name: __MODULE__)
  end

  def init(_opts) do
    Logger.info "TriggerWorker started"
    {:ok, conn} = Connection.open
    {:ok, channel} = Channel.open(conn)

    Queue.declare channel, @queue
    Exchange.declare channel, @exchange, :fanout
    Queue.bind channel, @queue, @exchange
    Queue.subscribe channel, @queue, fn (payload, _meta) ->
      Logger.debug "trigger worker: #{payload}"
      case payload do
        "measures:new:" <> measure_id ->
          spawn fn -> process_new_measure(measure_id) end
      end
    end
    {:ok, channel}
  end

  def process_new_measure(measure_id) do
    alias IotData.{Repo, Trigger, Measure}
    triggers = Repo.all(Trigger)
    m = Repo.get!(Measure, measure_id)
    Enum.each(triggers, fn r ->
      if Trigger.evaluate(r, m.value) == true do
        Trigger.call_action(r, m.value)
      end
    end)
  end
end
