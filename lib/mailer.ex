defmodule IotData.Mailer do
  @config domain: Application.get_env(:mailgun, :mailgun_domain),
          key: Application.get_env(:mailgun, :mailgun_key)
  use Mailgun.Client, @config

	@from "Cuybots <hola@cuybots.com>"

  def send_message(subject, recipient, body) do
    send_email to: recipient,
               from: @from,
               subject: subject,
               html: body
  end
end
