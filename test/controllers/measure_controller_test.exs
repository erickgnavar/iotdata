defmodule IotData.MeasureControllerTest do
  use IotData.ConnCase

  alias IotData.{Feed, Measure}
  @invalid_attrs %{}

  setup %{conn: conn} do
    feed = Repo.insert!(%Feed{name: "test feed"})
    {:ok, conn: put_req_header(conn, "accept", "application/json"), feed: feed}
  end

  test "lists all entries on index", %{conn: conn, feed: feed} do
    conn = get conn, measure_path(conn, :index, feed.id)
    assert json_response(conn, 200)["data"] == []
  end

  test "creates and renders resource when data is valid", %{conn: conn, feed: feed} do
    value = 20
    conn = post conn, measure_path(conn, :create, feed.id), feed_id: feed.id, value: value
    assert json_response(conn, 201)["data"]["id"]
    assert Repo.get_by(Measure, [
      feed_id: feed.id,
      value: value
    ])
  end

  test "does not create resource and renders errors when data is invalid", %{conn: conn, feed: feed} do
    conn = post conn, measure_path(conn, :create, feed.id), feed_id: nil, value: nil
    assert json_response(conn, 422)["errors"] != %{}
  end
end
