defmodule IotData.EmailActionControllerTest do
  use IotData.ConnCase

  alias IotData.EmailAction
  @valid_attrs %{body: "some content", recipient: "some content", subject: "some content"}
  @invalid_attrs %{}

  test "lists all entries on index", %{conn: conn} do
    conn = get conn, email_action_path(conn, :index)
    assert html_response(conn, 200) =~ "Listing email actions"
  end

  test "renders form for new resources", %{conn: conn} do
    conn = get conn, email_action_path(conn, :new)
    assert html_response(conn, 200) =~ "New email action"
  end

  test "creates resource and redirects when data is valid", %{conn: conn} do
    conn = post conn, email_action_path(conn, :create), email_action: @valid_attrs
    assert redirected_to(conn) == email_action_path(conn, :index)
    assert Repo.get_by(EmailAction, @valid_attrs)
  end

  test "does not create resource and renders errors when data is invalid", %{conn: conn} do
    conn = post conn, email_action_path(conn, :create), email_action: @invalid_attrs
    assert html_response(conn, 200) =~ "New email action"
  end

  test "shows chosen resource", %{conn: conn} do
    email_action = Repo.insert! %EmailAction{}
    conn = get conn, email_action_path(conn, :show, email_action)
    assert html_response(conn, 200) =~ "Show email action"
  end

  test "renders page not found when id is nonexistent", %{conn: conn} do
    assert_error_sent 404, fn ->
      get conn, email_action_path(conn, :show, -1)
    end
  end

  test "renders form for editing chosen resource", %{conn: conn} do
    email_action = Repo.insert! %EmailAction{}
    conn = get conn, email_action_path(conn, :edit, email_action)
    assert html_response(conn, 200) =~ "Edit email action"
  end

  test "updates chosen resource and redirects when data is valid", %{conn: conn} do
    email_action = Repo.insert! %EmailAction{}
    conn = put conn, email_action_path(conn, :update, email_action), email_action: @valid_attrs
    assert redirected_to(conn) == email_action_path(conn, :show, email_action)
    assert Repo.get_by(EmailAction, @valid_attrs)
  end

  test "does not update chosen resource and renders errors when data is invalid", %{conn: conn} do
    email_action = Repo.insert! %EmailAction{}
    conn = put conn, email_action_path(conn, :update, email_action), email_action: @invalid_attrs
    assert html_response(conn, 200) =~ "Edit email action"
  end

  test "deletes chosen resource", %{conn: conn} do
    email_action = Repo.insert! %EmailAction{}
    conn = delete conn, email_action_path(conn, :delete, email_action)
    assert redirected_to(conn) == email_action_path(conn, :index)
    refute Repo.get(EmailAction, email_action.id)
  end
end
