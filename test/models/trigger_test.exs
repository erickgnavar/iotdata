defmodule IotData.TriggerTest do
  use IotData.ModelCase

  alias IotData.Trigger

  @valid_attrs %{action_id: 42, action_type: "some content", comparison_value: "some content", condition: "some content", data_type: "some content", name: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Trigger.changeset(%Trigger{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Trigger.changeset(%Trigger{}, @invalid_attrs)
    refute changeset.valid?
  end
end
