defmodule IotData.MeasureTest do
  use IotData.ModelCase

  alias IotData.Measure

  @valid_attrs %{value: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Measure.changeset(%Measure{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Measure.changeset(%Measure{}, @invalid_attrs)
    refute changeset.valid?
  end
end
