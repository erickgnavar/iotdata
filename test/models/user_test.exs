defmodule IotData.UserTest do
  use IotData.ModelCase

  alias IotData.User

  @valid_attrs %{api_key: "some content", email: "some content", first_name: "some content", last_name: "some content", password_hash: "some content", time_zone: "some content", username: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = User.changeset(%User{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = User.changeset(%User{}, @invalid_attrs)
    refute changeset.valid?
  end
	  test "hash password only when changeset is valid" do
    changeset = User.registration_changeset(%User{}, %{
      email: "demo",
      password: "secret"
    })
    refute changeset.valid?
    refute Map.has_key? changeset.changes, :password_hash
  end

  test "hash password when changeset is valid" do
    changeset = User.registration_changeset(%User{}, %{
      email: "demo@demo.com",
      password: "secret"
    })
    assert changeset.valid?
    assert Map.has_key? changeset.changes, :password_hash
    assert Comeonin.Bcrypt.checkpw("secret", changeset.changes.password_hash)
  end
end
