defmodule IotData.EmailActionTest do
  use IotData.ModelCase

  alias IotData.EmailAction

  @valid_attrs %{body: "some content", recipient: "some content", subject: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = EmailAction.changeset(%EmailAction{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = EmailAction.changeset(%EmailAction{}, @invalid_attrs)
    refute changeset.valid?
  end
end
