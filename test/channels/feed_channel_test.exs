defmodule IotData.FeedChannelTest do
  use IotData.ChannelCase

  alias IotData.FeedChannel

  setup do
    {:ok, _, socket} =
      socket("user_id", %{some: :assign})
      |> subscribe_and_join(FeedChannel, "feeds:1")

    {:ok, socket: socket}
  end
end
