defmodule IotData.Repo.Migrations.CreateTrigger do
  use Ecto.Migration

  def change do
    create table(:triggers) do
      add :name, :string
      add :data_type, :string
      add :condition, :string
      add :comparison_value, :string
      add :action_type, :string
      add :action_id, :integer
      add :user_id, references(:users, on_delete: :delete_all), null: false
      add :feed_id, references(:feeds, on_delete: :delete_all), null: false

      timestamps
    end
    create index(:triggers, [:user_id])
    create index(:triggers, [:feed_id])

  end
end
