defmodule IotData.Repo.Migrations.CreateMeasure do
  use Ecto.Migration

  def change do
    create table(:measures) do
      add :value, :string
      add :feed_id, references(:feeds, on_delete: :delete_all), null: false

      timestamps
    end
    create index(:measures, [:feed_id])

  end
end
