defmodule IotData.Repo.Migrations.CreateEmailAction do
  use Ecto.Migration

  def change do
    create table(:email_actions) do
      add :recipient, :string
      add :subject, :string
      add :body, :text
      add :user_id, references(:users, on_delete: :delete_all), null: false

      timestamps
    end
    create index(:email_actions, [:user_id])

  end
end
