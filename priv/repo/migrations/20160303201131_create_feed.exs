defmodule IotData.Repo.Migrations.CreateFeed do
  use Ecto.Migration

  def change do
    create table(:feeds) do
      add :name, :string
      add :description, :string
      add :latitude, :float
      add :longitude, :float
      add :api_key, :string
      add :user_id, references(:users, on_delete: :delete_all), null: false

      timestamps
    end
    create index(:feeds, [:user_id])

  end
end
